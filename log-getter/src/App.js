import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
        Log Info Getter
          <img src={logo} className="App-logo" alt="logo" />
          {/* <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a> */}
        </header>
        <section class = "section">
          <nav className="App-nav">
            Filters!
          </nav>
          <article className="App-article">
            <h1>
              LOGGGG HEADER
            </h1>
            <p>
              Bleeeeehhh
            </p>
          </article>
        </section>
        <footer className="App-footer">
          <h2>Some footer here</h2>
        </footer>
      </div>
      
    );
  }
}

class HelloMessage extends React.Component {
  render() {
    return (
      <div>
        HelloMessage
      </div>
    )
  }
}


export default App;
